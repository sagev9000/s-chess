//
// Created by sage on 2/1/23.
//

#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#include "score.h"

#define DEFAULT_SCORE (-999)
#define BASE_SCORE 10000

int piece_score(char square) {
    switch (square) {
        case BPAWN:
        case WPAWN:
            return 1;
        case BKNIGHT:
        case WKNIGHT:
            return 2;
        case BBISHOP:
        case WBISHOP:
            return 25;
        case BROOK:
        case WROOK:
            return 3;
        case BQUEEN:
        case WQUEEN:
            return 5;
        case BKING:
        case WKING:
            return 2000;
        default:
            return 0;
    }
}

/// For each piece the player controls,
/// create a board based on each valid move,
/// score that board,
/// and return the highest-scoring board, with
/// it now being the other player's turn
Scored find_highest_score(Board board, int (*score)(Board*, long, RecursiveScorer*), long depth, RecursiveScorer* scorer) {
    int top_score = DEFAULT_SCORE;
    Board top_board;
    int length;
    Move* legal_moves = get_legal_moves(&board, &length);
    for (int i = 0; i < length; i++) {
        Board clone = board;
        apply_move(&clone, legal_moves[i]);
        int clone_score = score(&clone, depth, scorer);
        // if (depth == DEFAULT_DEPTH) {
        //     printf("Top-level score: %d\n", clone_score);
        // }
        if (clone_score > top_score) {
            top_score = clone_score;
            top_board = clone;
            scorer->line[DEFAULT_DEPTH - depth].move = legal_moves[i];
        }
    }
    free(legal_moves);
    return (Scored){ .board = top_board, .score = top_score };
}

#define MAP_SIZE 20000003

int unused_maps(RecursiveScorer* scorer) {
    int unused = 0;
    for (int i = 0; i < MAP_SIZE; i++) {
        if (scorer->map[i] == DEFAULT_SCORE) {
            unused++;
        }
    }
    return unused;
}

unsigned long board_hash(Board* board, long depth, size_t map_size) {
    unsigned long hash = 5381;
    for (int row = 0; row < 8; row++) {
        for (int col = 0; col < 8; col++) {
            hash = ((hash << 5) + hash) + board->squares[col][row];
        }
    }
    hash = ((hash << 5) + hash) + board->player;
    hash = ((hash << 5) + hash) + depth;
    return hash % map_size;
}

long unique_scorings = 0;
long reused_scorings = 0;
long hash_watch = 0;
long hash_checks = 0;
long hash_incorrect = 0;

int base_scorer(Board* board) {
    int score = BASE_SCORE;
    for (int col = 0; col < 8; col++) {
        for (int row = 0; row < 8; row++) {
            char sq = board->squares[col][row];
            int friendly_mult = iswhite(sq) == (board->player == 'W') ? 1 : -1;
            int value = piece_score(sq);
            score += (value * friendly_mult);
        }
    }
    return score;
}

int r_piece_count_scorer(Board* board, long depth, RecursiveScorer* scorer);

int r_piece_count_scorer(Board *board, long depth, RecursiveScorer *scorer) {
    if (depth <= 0) {
        int maximizing_player = depth % 2 == DEFAULT_DEPTH % 2;
        if (maximizing_player) {
            return base_scorer(board);
        }
        return (BASE_SCORE * 2) - base_scorer(board);
    }

    depth--;

    unsigned long hash = board_hash(board, depth, scorer->map_size);
    hash_watch++;
    int verify_hash = hash_watch % 100 == 0;
    if (scorer->map[hash] != DEFAULT_SCORE) {
        reused_scorings++;
        if (!verify_hash) {
            return scorer->map[hash];
        }
        Scored best_enemy_move = find_highest_score(*board, r_piece_count_scorer, depth, scorer);
        int score = (BASE_SCORE * 2) - best_enemy_move.score;
        hash_checks++;
        if (score != scorer->map[hash]) {
            hash_incorrect++;
        }
    } else {
        unique_scorings++;
    }

    Scored best_enemy_move = find_highest_score(*board, r_piece_count_scorer, depth, scorer);
    int score = (BASE_SCORE * 2) - best_enemy_move.score;
    scorer->map[hash] = score;
    return score;
}

RecursiveScorer new_recursive_scorer(ssize_t map_size) {
    RecursiveScorer rs = {
            .map_size = map_size,
            .map = calloc(map_size, sizeof(SCORE_TYPE)),
    };
    for (int i = 0; i < 20000000; i++) {
        rs.map[i] = DEFAULT_SCORE;
    }
    for (int i = 0; i < DEFAULT_DEPTH; i++) {
        rs.line[i].score = 0;
    }
    return rs;
}

Board move_with_recursive_search(Board board, RecursiveScorer* scorer) {
    long depth = DEFAULT_DEPTH;
    clock_t t = clock();
    printf("\nMoving as %s:\n", iswhite(board.player) ? "White" : "Black");
    Scored scored = find_highest_score(board, r_piece_count_scorer, depth, scorer);
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC;
    //printf("\r%'ld possible moves calculated in %f seconds\n", possible_moves_calculated, time_taken);
    printf("Moves calculated in %f seconds\n", time_taken);
    // possible_moves_calculated = 0;
    printf("Score: %d\n", scored.score);
    printf("   Total hashes: %'ld\n", hash_checks);
    printf("Unique scorings: %'ld\n", unique_scorings);
    // printf("Re-used scorings: %ld\n", reused_scorings);
    //printf("    Unused maps: %'d\n", unused_maps());
    double rate = ((double) hash_incorrect) / ((double) hash_checks);
    hash_checks = 0;
    hash_incorrect = 0;
    printf("Failed hash rate: %f%%\n", rate * 100);
    return scored.board;
}

void explain_line(RecursiveScorer* scorer) {
    for (int i = 0; i < DEFAULT_DEPTH; i++) {
        if (i % 2 == 0) {
            printf("     I move ");
        } else {
            printf("Enemy moves ");
        }
        printm(scorer->line[i].move);
    }
}