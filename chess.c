#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

#include "chess.h"
#include "score.h"

void printm(Move move) {
    printf("from (%d,%d) to (%d,%d)\n", move.from_x, move.from_y, move.to_x, move.to_y);
}

char starting_squares[8][8] = {
        {BROOK, BPAWN, EMPTY, EMPTY, EMPTY, EMPTY, WPAWN, WROOK},
        {BKNIGHT, BPAWN, EMPTY, EMPTY, EMPTY, EMPTY, WPAWN, WKNIGHT},
        {BBISHOP, BPAWN, EMPTY, EMPTY, EMPTY, EMPTY, WPAWN, WBISHOP},
        {BQUEEN, BPAWN, EMPTY, EMPTY, EMPTY, EMPTY, WPAWN, WQUEEN},
        {BKING, BPAWN, EMPTY, EMPTY, EMPTY, EMPTY, WPAWN, WKING},
        {BBISHOP, BPAWN, EMPTY, EMPTY, EMPTY, EMPTY, WPAWN, WBISHOP},
        {BKNIGHT, BPAWN, EMPTY, EMPTY, EMPTY, EMPTY, WPAWN, WKNIGHT},
        {BROOK, BPAWN, EMPTY, EMPTY, EMPTY, EMPTY, WPAWN, WROOK},
};

int isblack(int piece) {
    return piece >= 'a' && piece <= 'z';
}

int iswhite(int piece) {
    return piece >= 'A' && piece <= 'Z';
}

const char* get_sq_name(const char square) {
    switch (square) {
        case WPAWN:
            return "White pawn";
        case WKNIGHT:
            return "White knight";
        case WBISHOP:
            return "White bishop";
        case WROOK:
            return "White rook";
        case WQUEEN:
            return "White queen";
        case WKING:
            return "White king";

        case BPAWN:
            return "Black pawn";
        case BKNIGHT:
            return "Black knight";
        case BBISHOP:
            return "Black bishop";
        case BROOK:
            return "Black rook";
        case BQUEEN:
            return "Black queen";
        case BKING:
            return "Black king";

        case EMPTY:
            return "Blank space";

        default:
            return NULL;
    }
}

Color get_color(int square) {
    if (iswhite(square)) {
        return WHITE;
    }
    if (isblack(square)) {
        return BLACK;
    }
    return BLANK;
}

void apply_move(Board* board, Move move) {
    board->squares[move.to_x][move.to_y] = board->squares[move.from_x][move.from_y];
    board->squares[move.from_x][move.from_y] = EMPTY;
    board->last_x = move.from_x;
    board->last_y = move.from_y;
    board->player = board->player == BKING ? WKING : BKING;
}

int are_enemies(int pieceA, int pieceB) {
    Color colorA = get_color(pieceA);
    Color colorB = get_color(pieceB);
    return colorA != colorB && colorA != BLANK && colorB != BLANK;
}

int knight_offsets[8][2] = {
        { 2, 1 },
        { 1, 2 },
        { -1, 2 },
        { -2, 1 },
        { -2, -1 },
        { -1, -2 },
        { 1, -2 },
        { 2, -1 },
};

int king_offsets[9][2] = {
        { -1, -1 },
        { -1, 1 },
        { -1, 0 },

        { 0, 1 },
        { 0, -1 },

        { 1, -1 },
        { 1, 1 },
        { 1, 0 },
};

struct move_itr {
    Move** itr;
    const Board* board;
    int row;
    int col;
};

void add_move(struct move_itr itr, int to_x, int to_y) {
    **itr.itr = (Move) {
            .from_x = itr.col,
            .from_y = itr.row,
            .to_x = to_x,
            .to_y = to_y,
    };
    (*itr.itr) = (*itr.itr) + 1;
}

int oob(int x, int y) {
    return x < 0 || x >= 8 || y < 0 || y >= 8;
}

typedef enum AddOutcome {
    OOB,
    FRIENDLY,
    IS_EMPTY,
    IS_ENEMY,
} AddOutcome;

AddOutcome try_add_basic_move(struct move_itr itr, int to_x, int to_y) {
    if (oob(to_x, to_y)) {
        return OOB;
    }
    char target = itr.board->squares[to_x][to_y];
    if (target == EMPTY || are_enemies(itr.board->squares[itr.col][itr.row], target)) {
        add_move(itr, to_x, to_y);
        return target == EMPTY ? IS_EMPTY : IS_ENEMY;
    }
    return FRIENDLY;
}

void add_right_angles(struct move_itr itr, int row, int col) {
    for (int x = col + 1; x < 8; x++) {
        if (try_add_basic_move(itr, x, row) != IS_EMPTY) {
            break;
        }
    }
    for (int x = col - 1; x >= 0; x--) {
        if (try_add_basic_move(itr, x, row) != IS_EMPTY) {
            break;
        }
    }
    for (int y = row + 1; y < 8; y++) {
        if (try_add_basic_move(itr, col, y) != IS_EMPTY) {
            break;
        }
    }
    for (int y = row - 1; y >= 0; y--) {
        if (try_add_basic_move(itr, col, y) != IS_EMPTY) {
            break;
        }
    }
}

void add_diagonals(struct move_itr itr, int row, int col) {
    for (int x = col + 1, y = row + 1; x < 8 && y < 8; x++, y++) {
        if (try_add_basic_move(itr, x, y) != IS_EMPTY) {
            break;
        }
    }
    for (int x = col - 1, y = row + 1; x >= 0 && y < 8; x--, y++) {
        if (try_add_basic_move(itr, x, y) != IS_EMPTY) {
            break;
        }
    }
    for (int x = col - 1, y = row - 1; x >= 0 && y >= 0; x--, y--) {
        if (try_add_basic_move(itr, x, y) != IS_EMPTY) {
            break;
        }
    }
    for (int x = col + 1, y = row - 1; x < 8 && y >= 0; x++, y--) {
        if (try_add_basic_move(itr, x, y) != IS_EMPTY) {
            break;
        }
    }
}

/// FREE ME
Move* get_legal_moves(const Board *board, int* length) {
    Move* move_list = malloc(sizeof(Move) * 256);
    Move* move_ptr = move_list;
    for (int row = 0; row < 8; row++) {
        for (int col = 0; col < 8; col++) {
            char square = (char) board->squares[col][row];
            if (square == EMPTY) {
                continue;
            }
            if (are_enemies(square, board->player)) {
                continue;
            }
            struct move_itr itr = { .row = row, .col = col, .itr = &move_ptr, .board = board };
            switch (square) {
                case BPAWN:
                case WPAWN: {
                    int direction = square == BPAWN ? 1 : -1;
                    if (board->squares[col][row + direction] == EMPTY) {
                        add_move(itr, col, row + direction);
                        if ((square == BPAWN && row == 1) || (square == WPAWN && row == 6)) {
                            if (board->squares[col][row + (direction * 2)] == EMPTY) {
                                add_move(itr, col, row + (direction * 2));
                            }
                        }
                    }
                    if (!oob(col + 1, row + direction) && are_enemies(square, board->squares[col + 1][row + direction])) {
                        add_move(itr, col + 1, row + direction);
                    }
                    if (!oob(col - 1, row + direction) && are_enemies(square, board->squares[col - 1][row + direction])) {
                        add_move(itr, col - 1, row + direction);
                    }
                    break;
                }
                case BKNIGHT:
                case WKNIGHT:
                    for (int i = 0; i < 8; i++) {
                        int x = knight_offsets[i][0] + col;
                        int y = knight_offsets[i][1] + row;
                        try_add_basic_move(itr, x, y);
                    }
                    break;
                case BKING:
                case WKING:
                    for (int i = 0; i < 9; i++) {
                        int x = king_offsets[i][0] + col;
                        int y = king_offsets[i][1] + row;
                        try_add_basic_move(itr, x, y);
                    }
                    break;
                case BROOK:
                case WROOK:
                    add_right_angles(itr, row, col);
                    break;
                case BBISHOP:
                case WBISHOP:
                    add_diagonals(itr, row, col);
                    break;
                case BQUEEN:
                case WQUEEN:
                    add_right_angles(itr, row, col);
                    add_diagonals(itr, row, col);
                    break;
            }
        }
    }
    *length = move_ptr - move_list;
    return move_list;
}

void printb(const Board *board) {
    printf("    0 1 2 3 4 5 6 7  \n");
    printf("  +-----------------+\n");
    for (int row = 0; row < 8; row++) {
        printf("%d |", row);
        for (int col = 0; col < 8; col++) {
            if (board->last_y == row && board->last_x == col) {
                printf(" *");
            } else {
                char square = board->squares[col][row];
                if (isblack(square)) {
                    printf("[32m");
                } else {
                    printf("[34m");
                }
                printf(" %c", square);
                printf("[0m");
            }
            //printf(" %c", board->squares[col][row]);
        }
        printf(" |\n");
    }
    printf("  +-----------------+\n\n");
}

char custom_starting_squares[8][8] = {
        {EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
        {EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
        {EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
        {EMPTY, BKING, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
        {EMPTY, WPAWN, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
        {EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
        {EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
        {EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
};

#define BOARD starting_squares

Board starting_board() {
    Board board;
    board.player = 'K';
    board.last_y = -1;
    board.last_x = -1;
    memcpy(&board.squares, &BOARD, sizeof(BOARD));
    return board;
}

Board middle_guy(char square) {
    Board b = starting_board();
    b.player = isblack(square) ? BKING : WKING;
    for (int row = 0; row < 8; row++) {
        for (int col = 0; col < 8; col++) {
            b.squares[col][row] = EMPTY;
        }
    }
    b.squares[3][4] = square;
    return b;
}

int are_equivalent_moves(Move a, Move b) {
    return a.from_x == b.from_x
           && a.from_y == b.from_y
           && a.to_x == b.to_x
           && a.to_y == b.to_y;
}

void user_move(Board* board);
void user_move(Board* board) {
    int length;
    Move* move_list = get_legal_moves(board, &length);
    int from_x, from_y, to_x, to_y;
    printf("Your turn:\n");
    scanf("%d %d %d %d", &from_x, &from_y, &to_x, &to_y);
    Move move = (Move) {
            .from_x = from_x,
            .from_y = from_y,
            .to_x = to_x,
            .to_y = to_y,
    };
    for (int i = 0; i < length; i++) {
        if (are_equivalent_moves(move, move_list[i])) {
            apply_move(board, move);
            return;
        }
    }
    printf("Not a legal move! Please try again.\n");
    user_move(board);
}

// TODO: Should be in_checkmate(), ha
int has_both_kings(Board* board) {
    int has_white = 0;
    int has_black = 0;
    for (int row = 0; row < 8; row++) {
        for (int col = 0; col < 8; col++) {
            if (board->squares[col][row] == WKING) {
                has_white = 1;
            } else if (board->squares[col][row] == BKING) {
                has_black = 1;
            }
        }
    }
    return has_white && has_black;
}

int main() {
    setlocale(LC_NUMERIC, "");
    Board board = starting_board();
    board.player = BKING;
    printb(&board);

    RecursiveScorer scorer_a = new_recursive_scorer(19999999);
    //RecursiveScorer scorer_b = new_recursive_scorer(20000003);
    RecursiveScorer* scorer = &scorer_a;
    while (has_both_kings(&board)) {
        board = move_with_recursive_search(board, &scorer_a);
        printb(&board);
        explain_line(&scorer_a);
        // scorer = (scorer == &scorer_a) ? &scorer_a : &scorer_a;
        user_move(&board);
        printb(&board);
    }
    printf("\n%s wins!:\n", isblack(board.player) ? "White" : "Black");

    return 0;
}
