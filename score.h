//
// Created by sage on 2/1/23.
//

#ifndef CCHESS_SCORE_H
#define CCHESS_SCORE_H

#define DEFAULT_DEPTH 5

#include <glob.h>
#include "chess.h"

#define SCORE_TYPE int

typedef struct ScoredMove {
    SCORE_TYPE score;
    Move move;
} ScoredMove;

typedef struct RecursiveScorer {
    size_t map_size;
    int* map;
    ScoredMove line[DEFAULT_DEPTH];
} RecursiveScorer;

RecursiveScorer new_recursive_scorer(ssize_t map_size);

Board move_with_recursive_search(Board board, RecursiveScorer* scorer);

void explain_line(RecursiveScorer* scorer);

#endif //CCHESS_SCORE_H
