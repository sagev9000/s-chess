//
// Created by sage on 2/1/23.
//

#ifndef CCHESS_CHESS_H
#define CCHESS_CHESS_H

#define WPAWN 'P'
#define WKNIGHT 'N'
#define WBISHOP 'B'
#define WROOK 'R'
#define WQUEEN 'Q'
#define WKING 'K'

#define BPAWN 'p'
#define BKNIGHT 'n'
#define BBISHOP 'b'
#define BROOK 'r'
#define BQUEEN 'q'
#define BKING 'k'

#define EMPTY ' '

#define FOR(_square_name, _board) for (int row = 0, col = 0, i = 0, _square_name = (unsigned char) (_board)->squares[row][col]; i < 64; i++, col = i % 8, row = i / 8, _square_name = (unsigned char) (_board)->squares[row][col])

typedef enum Color {
    BLACK,
    WHITE,
    BLANK
} Color;

typedef struct Move {
    signed char from_x;
    signed char from_y;
    signed char to_x;
    signed char to_y;
} Move;

typedef struct Board {
    char squares[8][8];
    char player;
    char last_x;
    char last_y;
} Board;

typedef struct Scored {
    Board board;
    int score;
} Scored;

void printb(const Board *board);
void printm(Move move);

int isblack(int piece);
int iswhite(int piece);

/// Free me
Move* get_legal_moves(const Board *board, int* length);

void apply_move(Board* board, Move move);

#endif //CCHESS_CHESS_H
