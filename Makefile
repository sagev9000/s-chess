all: chess.c score.c
	gcc -g -Wall -o chess chess.c score.c -I.

run: all
	./chess

clean:
	rm ./chess
